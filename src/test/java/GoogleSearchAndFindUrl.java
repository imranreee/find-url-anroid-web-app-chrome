import io.appium.java_client.android.AndroidDriver;
import org.omg.PortableServer.THREAD_POLICY_ID;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.net.URL;

/**
 * Created by: Al Imran on 12/10/2018.
 * Email: imranreee@gmail.com
 **/

public class GoogleSearchAndFindUrl {
    WebDriver driver;
    String baseUrl = "http://www.google.com";
    String myIpUrl = "http://www.myip.com";
    String appPackage = "com.android.settings";
    String appActivity = "com.android.settings.Settings";
    String excelPath = System.getProperty("user.dir") + "\\excel\\IPAddress.xlsx";
    String chromeDriverPath = System.getProperty("user.dir") + "\\chromedriver\\chromedriver.exe";

    //Put the values
    int howManyLoop = 3;
    String androidVersion = "8.1";
    String searchString = "facebook";
    String expectedUrl = "https://m.facebook.com"; //URL example "https://m.facebook.com"

    @Test()
    public void googleSearch() throws Exception {
        for (int i = 0; i < howManyLoop; i++){
            Thread.sleep(3000);

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "Android");
            capabilities.setCapability("deviceVersion", androidVersion);
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("browserName", "Chrome");
            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
            driver.get(baseUrl);

            By googleSearchField = By.xpath("//input[@name='q']");
            By searchBtn = By.xpath("//span[@class='z1asCe MZy1Rb']//*[@focusable='false']");

            String fullPath = "//span[contains(text(),'"+expectedUrl+"')]";
            By expUrl = By.xpath(fullPath);
            //By expUrl = By.xpath("//span[contains(text(),'https://m.facebook.com')]");
            By networkSettings = By.xpath("//*[@text='Network & Internet']");
            By aeroplaneMode = By.id("android:id/switch_widget");

            waitForVisibilityOf(googleSearchField);
            driver.findElement(googleSearchField).click();
            driver.findElement(googleSearchField).sendKeys(searchString);
            Thread.sleep(2000);
            driver.findElement(searchBtn).click();
            Thread.sleep(3000);
            Thread.sleep(3000);
            Thread.sleep(3000);

            if (driver.findElements(expUrl).size() > 0){
                String expectedUrlStr = driver.findElement(expUrl).getText();
                System.out.println("Expected url found: "+expectedUrlStr);
                ExcelWrite.writeData(excelPath, 0, i+2, 2, "Yes");

                driver.findElement(expUrl).click();
            }else {
                System.out.println("Expected URL not found :(");
                ExcelWrite.writeData(excelPath, 0, i+2, 2, "No");
            }
            driver.quit();
            System.out.println("Browser closed");


            //===========================
            //Aeroplane mode turn off and on

            DesiredCapabilities dc = new DesiredCapabilities();
            dc.setCapability("deviceName", "Android");
            dc.setCapability("deviceVersion", androidVersion);
            dc.setCapability("platformName", "Android");
            dc.setCapability("appPackage", appPackage);
            dc.setCapability("appActivity", appActivity);
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);

            waitForVisibilityOf(networkSettings);
            System.out.println("Network settings found");

            driver.findElement(networkSettings).click();
            waitForVisibilityOf(aeroplaneMode);
            System.out.println("Aeroplane mode settings found");

            driver.findElement(aeroplaneMode).click();
            System.out.println("Turned on Aeroplane mode");
            Thread.sleep(3000);
            Thread.sleep(3000);

            driver.findElement(aeroplaneMode).click();
            System.out.println("Turned on Aeroplane mode");
            Thread.sleep(3000);

            driver.quit();
            System.out.println("Settings app closed");


            //==========================
            //For collecting IP address

            DesiredCapabilities dc2 = new DesiredCapabilities();
            dc2.setCapability("deviceName", "Android");
            dc2.setCapability("deviceVersion", androidVersion);
            dc2.setCapability("platformName", "Android");
            dc2.setCapability("browserName", "Chrome");
            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc2);
            driver.get(myIpUrl);
            Thread.sleep(3000);
            System.out.println("Browser latched");

            By myIpAddress = By.xpath("//span[@id='ip']");
            waitForVisibilityOf(myIpAddress);

            String collectedIpAddress = driver.findElement(myIpAddress).getText();
            System.out.println("Current IP Address: "+collectedIpAddress);
            ExcelWrite.writeData(excelPath, 0, i+2, 3, collectedIpAddress);
            System.out.println("IP Address collected");

            driver.quit();
            System.out.println("Browser closed");
            System.out.println("=================== "+(i+1)+" no loop ended ===================");
        }
    }


    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
